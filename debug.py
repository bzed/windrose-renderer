#!/usr/bin/env python3
import sys
import os

sys.path = [os.path.realpath(os.path.dirname(__file__))] + sys.path
from web import app

if __name__ == '__main__':
    if len(sys.argv) > 1:
        host, port = sys.argv[1:]
        app.run(host=host, port=int(port), debug=True)
    else:
        app.run(debug=True)

