FROM debian:stable-slim

MAINTAINER Bernd Zeimetz "bernd@bzed.de"

RUN apt update -y && apt dist-upgrade -y
RUN apt install -y python3 gunicorn python3-pip python3-setuptools

COPY requirements.txt /
RUN pip3 install -r /requirements.txt
COPY . /app
WORKDIR /app
EXPOSE 8765/tcp
ENTRYPOINT ["./run-gunicorn.sh"]

