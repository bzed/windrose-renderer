import os
import time
from datetime import datetime
from flask import Flask, redirect, request, Response
from prometheus_flask_exporter import PrometheusMetrics
from prometheus_flask_exporter.multiprocess import \
        GunicornInternalPrometheusMetrics
from flask_healthcheck import Healthcheck
from prometheus_pandas import query
from windrose import wrscatter
from werkzeug.exceptions import BadRequest
import pandas as pd
from matplotlib.figure import Figure
from io import StringIO
import numpy as np
from math import pi, log10
import matplotlib.style as mp_style

mp_style.use('https://raw.githubusercontent.com/quantumblacklabs/qbstyles/master/qbstyles/styles/qb-common.mplstyle') # noqa
mp_style.use('https://raw.githubusercontent.com/quantumblacklabs/qbstyles/master/qbstyles/styles/qb-dark.mplstyle') # noqa


DEFAULT_REDIRECT = 'https://weather.bzed.org/'
PROMETHEUS_URL = 'http://data001.bzed.at:9091'

app = Flask(__name__)
Healthcheck(app)
if 'PROMETHEUS_MULTIPROC_DIR' in os.environ:
    metrics = GunicornInternalPrometheusMetrics(app)
else:
    metrics = PrometheusMetrics(app)

# static information as metric
# metrics.info('app_info', 'Application info', version='1.0.3')


def __wind_color__(windspeed):
    # green rgb(55, 135, 45)
    # yellow
    colour = 'tab:green'
    if windspeed > 15:
        colour = 'yellow'
    if windspeed > 25:
        colour = 'red'
    return colour


def __alpha__(epoch, max_epoch, min_epoch):
    l_min_epoch = log10(min_epoch)
    l_max_epoch = log10(max_epoch)
    l_epoch = log10(epoch)
    bucket_size = (l_max_epoch-l_min_epoch) / 100.0
    bucket = 10 ** ((l_epoch - l_min_epoch) / bucket_size)
    return bucket / 100


def __epoch_to_alpha__(df):
    min_epoch = df.min()
    max_epoch = df.max()
    bins = 10 ** np.linspace(np.log10(min_epoch), np.log10(max_epoch), 100)
    bins = bins.tolist()
    bins.append(np.inf)
    bins[0] = 0
    labels = [x / 100 for x in range(1, 101)]
    r_df = pd.cut(df, bins, labels=labels)
    return r_df.astype('float')


@app.errorhandler(BadRequest)
def handle_bad_request(e):
    return e, 400


@app.route('/')
def index():
    return redirect(DEFAULT_REDIRECT)


@app.route('/render/')
def render():
    try:
        width = request.args.get('width', default=300, type=int)
        height = request.args.get('height', default=300, type=int)
        location = request.args.get('location', default='Gaisberg', type=str)
        start = request.args.get(
                'start',
                default=int(time.time() - 15*60),
                type=int
                )
        end = request.args.get('end', default=int(time.time()), type=int)

        start_iso = '{}Z'.format(
                datetime.utcfromtimestamp(start).isoformat(timespec='seconds')
                )
        end_iso = '{}Z'.format(
                datetime.utcfromtimestamp(end).isoformat(timespec='seconds')
                )

    except ValueError as e:
        raise BadRequest(e)

    p = query.Prometheus(PROMETHEUS_URL)
    results = []
    for mtype in ('speed', 'direction'):
        r = p.query_range(
                'meteohub_wind_{}{{location="{}"}}'.format(mtype, location),
                start_iso, end_iso, '3s'
                )
        r.columns = [mtype]
        results.append(r)
    wind_data = results[0].join(results[1], how='inner')
    pd.set_option("display.max_rows", 20, "display.max_columns", None)
    wind_data['direction'] = wind_data['direction'] * pi / 180
    wind_data['speed'] = wind_data['speed'] * 3.6
    wind_data['color'] = wind_data['speed'].transform(__wind_color__)
    wind_data['epoch'] = pd.to_datetime(wind_data.index).view('int64') / 1e9
    wind_data['alpha'] = __epoch_to_alpha__(wind_data['epoch'])
    print(wind_data)

    print(len(wind_data['speed']))
    print(len(wind_data['direction']))
    print(len(wind_data['color']))

    windrose = plot_windrose(
            speed=wind_data['speed'],
            direction=wind_data['direction'],
            color=wind_data['color'],
            alpha=wind_data['alpha'],
            height=height,
            width=width
            )
    return Response(windrose, mimetype='image/svg+xml')


def plot_windrose(speed, direction, color, alpha, height, width):
    fig = Figure()

    wrscatter(
            direction=direction,
            var=speed,
            rmax=None,
            fig=fig,
            color=color,
            alpha=alpha
            )
    # ax.patch.set_facecolor('black')
    wi, hi = fig.get_size_inches()
    dpi = height/hi
    fig.set_size_inches(hi*(width/height), hi)
    f = StringIO()
    fig.savefig(f, format='svg', dpi=dpi)
    return f.getvalue()
