import multiprocessing
from prometheus_flask_exporter.multiprocess import \
        GunicornInternalPrometheusMetrics


def child_exit(server, worker):
    GunicornInternalPrometheusMetrics.mark_process_dead_on_child_exit(
            worker.pid
    )


wsgi_app = 'web:app'
threads = 50
bind = '0.0.0.0:8765'
workers = multiprocessing.cpu_count() * 2 + 1
